/**
 * Request.java
 *
 * This class represents a message that is received
 * at the PLC-end, from Java applications.
 *
 * @author Lavindra de Silva
 * @version 1.0 
 */

package plcServer;

public class Request {

    /**
     * The types of messages, the kinds of data
     * structures that can be manipulated, etc.
     */
    
    public static final int WRITE = 0;
    public static final int READ = 1;
    
    public static final int BOOL = 2;
    public static final int SHORT = 3;
    public static final int STRING = 4;
    public static final int INT = 5;
    public static final int FLOAT = 6;
        
    public static final String LOCAL = "MAIN.";
    public static final String GLOBAL = "GLOBAL_VARIABLES.";
    
    /**
     * The following values are set once an
     * instance of this class is created.
     */
     
    public int operation;
    public int type;
    public String scope;
    public String variable;
    public String value;
    
    private String toStr = "";
    
    
    /**
     * Convert this message object into a string.
     */
    
    public String toString() {
    
        switch(operation) {
            case WRITE: toStr += "write"; break;
            case READ: toStr += "read"; break;
        }
        
        toStr += " the ";
                
        switch(type) {
            case BOOL: toStr += "boolean"; break;
            case SHORT: toStr += "short"; break;
            case STRING: toStr += "string"; break;
            case INT: toStr += "integer"; break;   
            case FLOAT: toStr += "float"; break;
        }

        if(value != null)
            toStr += " value '" + value + "' to the ";
        else
            toStr += " value from the ";
        
        switch(scope) {
            case GLOBAL: toStr += "global"; break;
            case LOCAL: toStr += "local"; break;
        }
        
        toStr += " variable named '" + variable +"'";

        return toStr;
    }
}
