/**
 * PLCServer.java
 *
 * This class is the interface to Java applications from
 * Beckhoff PLCs. This class is based on examples provided  
 * by Beckhoff for how to use 'TwinCAT TcAdsDLL'.
 *
 * @author Lavindra de Silva
 * @author Daniele Scrimieri
 *
 * @version 1.0 
 */

package plcServer;

import de.beckhoff.jni.Convert;
import de.beckhoff.jni.JNIByteBuffer;
import de.beckhoff.jni.tcads.AmsAddr;
import de.beckhoff.jni.tcads.AdsCallDllFunction;
import java.io.*;
import java.net.*;
import java.util.*;


/**
 * Note: A lot of the code in this class can be  
 * substantially reduced.
 */
  
public class PLCServer {

    /**
     * The port and the maximum number of
     * requests allowed before the server
     * shuts down.
     */
    
    private static int port = 0;
    private static final int ethPort = 850;
    private static final String prefix = "[Java PLC Server] ";
    private static final String REQUEST_ERROR = "ERROR_INVALID_REQUEST";
    private static final int MAX_REQUESTS = 1000;
    

    /**
     * The constructor.
     * @param port The port on which the server should listen for client/application connections.
     */
     
    public PLCServer(int port) {
        this.port = port;
    }
    

    /**
     * Read the value stored in a boolean variable on the PLC.
     * @param plcVar The name of the variable to read from.
     * @return The true/false value of the variable.
     */

    public ReturnValue readBool(String plcVar) {
    
        boolean ret = false;
        long err;
        ReturnValue retn = new ReturnValue();

        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar, true));
        JNIByteBuffer dataBuff = new JNIByteBuffer(1);        

        // Open communication with the PLC via the Beckhoff DLL
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        // Get handle by symbol name
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_HNDBYNAME,
                0x0,
                handleBuff.getUsedBytesCount(),
                handleBuff,
                symbolBuff.getUsedBytesCount(),
                symbolBuff);
                
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }

        retn.errCode = err;

        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());
        // Read value by handle
        err = AdsCallDllFunction.adsSyncReadReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_VALBYHND,
                hdlBuffToInt,
                0x4,
                dataBuff);
                
        if (err != 0) {
            System.out.println("Error: Read by handle: 0x" + Long.toHexString(err));
        } else {
            ret = Convert.ByteArrToBool(dataBuff.getByteArray());    
        }

        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
                
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        retn.boolVal = ret;
        return retn;
    }


    /**
     * Read the value stored in an integer variable on the PLC.
     * @param plcVar The name of the variable to read from.
     * @return The value that was read from the variable.
     */
    
    public ReturnValue readInt(String plcVar) {
    
        short ret = 0;
        long err;
        ReturnValue retn = new ReturnValue();
        
        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        JNIByteBuffer dataBuff = new JNIByteBuffer(2);        

        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        // Get handle by symbol name
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_HNDBYNAME,
                0x0,
                handleBuff.getUsedBytesCount(),
                handleBuff,
                symbolBuff.getUsedBytesCount(),
                symbolBuff);
                
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        retn.errCode = err;

        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());
        // Read value by handle
        err = AdsCallDllFunction.adsSyncReadReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_VALBYHND,
                hdlBuffToInt,
                0x4,
                dataBuff);
                
        if (err != 0) {
            System.out.println("Error: Read by handle: 0x" + Long.toHexString(err));
        } else {      
            ret = Convert.ByteArrToShort(dataBuff.getByteArray());             
        }

        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
                
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        retn.shortVal = ret;
        return retn;
    }


    /**
     * Read the value stored in a 'real' variable on the PLC.
     * @param plcVar The name of the variable to read from.
     * @return The value that was read from the variable.
     */
     
    public ReturnValue readReal(String plcVar) {
    
        float ret = 0;
        long err;
        ReturnValue retn = new ReturnValue();
        
        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        JNIByteBuffer dataBuff = new JNIByteBuffer(4);        

        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        // Get handle by symbol name
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_HNDBYNAME,
                0x0,
                handleBuff.getUsedBytesCount(),
                handleBuff,
                symbolBuff.getUsedBytesCount(),
                symbolBuff);
                
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        retn.errCode = err;

        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());
        // Read value by handle
        err = AdsCallDllFunction.adsSyncReadReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_VALBYHND,
                hdlBuffToInt,
                0x4,
                dataBuff);
                
        if (err != 0) {
            System.out.println("Error: Read by handle: 0x" + Long.toHexString(err));
        } else {      
            ret = Convert.ByteArrToFloat(dataBuff.getByteArray());             
        }

        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        //retn.errCode = err;
        retn.realVal = ret;
        System.out.println("TEMPORARY PRINTOHT:"+retn.realVal);
        return retn;
    }    
    

    /**
     * Write a boolean value to a variable on the PLC.
     * @param plcVar The name of the variable to write to.
     * @param value The value that is to be written to the variable.
     * @return Success or failure.
     */
     
    public ReturnValue writeBool(String plcVar, boolean value) {
    
        long err;
        ReturnValue ret = new ReturnValue();
        
        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar, true));
        JNIByteBuffer dataBuff = new JNIByteBuffer(Convert.BoolToByteArr(value));     
        
        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        //get handle by symbol name 
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr, 0xF003, 0x0,
                handleBuff.getUsedBytesCount(), handleBuff, //buffer for getting handle
                symbolBuff.getUsedBytesCount(), symbolBuff); //buffer containg symbolpath
                
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        ret.errCode = err;
   
        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());

        //write variable by handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr, 0xF005, hdlBuffToInt,
                dataBuff.getUsedBytesCount(), dataBuff);
   
        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
                
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        return ret;
    }
    
    
    /**
     * Write an integer value to a variable on the PLC.
     * @param plcVar The name of the variable to write to.
     * @param value The value that is to be written to the variable.
     * @return Success or failure.
     */    
    
    public ReturnValue writeInt(String plcVar, short value) {
    
        long err;
        ReturnValue ret = new ReturnValue();

        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar, true));
        JNIByteBuffer dataBuff = new JNIByteBuffer(Convert.ShortToByteArr(value));     
        
        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        //get handle by symbol name 
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr, 0xF003, 0x0,
                handleBuff.getUsedBytesCount(), handleBuff, //buffer for getting handle
                symbolBuff.getUsedBytesCount(), symbolBuff); //buffer containg symbolpath
                
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        ret.errCode = err;
   
        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());

        //write variable by handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr, 0xF005, hdlBuffToInt,
                dataBuff.getUsedBytesCount(), dataBuff);
   
        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
                
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        return ret;
    }

    
    /**
     * Write a 'real' value to a variable on the PLC.
     * @param plcVar The name of the variable to write to.
     * @param value The value that is to be written to the variable.
     * @return Success or failure.
     */    
    
    public ReturnValue writeReal(String plcVar, float value) {

        long err;
        ReturnValue ret = new ReturnValue();

        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar, true));
        JNIByteBuffer dataBuff = new JNIByteBuffer(Convert.FloatToByteArr(value));     
        
        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        //get handle by symbol name 
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr, 0xF003, 0x0,
                handleBuff.getUsedBytesCount(), handleBuff, //buffer for getting handle
                symbolBuff.getUsedBytesCount(), symbolBuff); //buffer containg symbolpath
                
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        ret.errCode = err;
   
        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());

        //write variable by handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr, 0xF005, hdlBuffToInt,
                dataBuff.getUsedBytesCount(), dataBuff);
   
        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
                
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        return ret;
    }
    
    
    /**
     * Read a string from a variable on the PLC.
     * @param plcVar The name of the variable to read from.
     * @return The value of the string.
     */    
    
    public ReturnValue readString(String plcVar) {
    
        ReturnValue retn = new ReturnValue();        
        String ret = "";
        long err;
        
        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        JNIByteBuffer dataBuff = new JNIByteBuffer(256);        

        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        // Get handle by symbol name
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_HNDBYNAME,
                0x0,
                handleBuff.getUsedBytesCount(),
                handleBuff,
                symbolBuff.getUsedBytesCount(),
                symbolBuff);
                
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }

        retn.errCode = err;

        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());
        // Read value by handle
        err = AdsCallDllFunction.adsSyncReadReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_VALBYHND,
                hdlBuffToInt,
                dataBuff.getUsedBytesCount(),
                dataBuff);
        if (err != 0) {
            System.out.println("Error: Read by handle: 0x" + Long.toHexString(err));
        } else {      
            ret = Convert.ByteArrToString(dataBuff.getByteArray());             
        }

        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
                
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        retn.strVal = ret;
        return retn;
    }

    
    /**
     * Write a string to a variable on the PLC.
     * @param plcVar The name of the variable to read from.
     * @return The value that is to be written.
     */       

    public ReturnValue writeString(String plcVar, String value) {
    
        ReturnValue ret = new ReturnValue();
        
        long err;
        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        JNIByteBuffer dataBuff = new JNIByteBuffer(Convert.StringToByteArr(value));     
        
        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        addr.setPort(port);
        
        //get handle by symbol name 
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr, 0xF003, 0x0,
                handleBuff.getUsedBytesCount(), handleBuff, //buffer for getting handle
                symbolBuff.getUsedBytesCount(), symbolBuff); //buffer containing symbolpath
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }

        ret.errCode = err;
  
        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());

        //write variable by handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr, 0xF005, hdlBuffToInt,
                dataBuff.getUsedBytesCount(), dataBuff);
        if (err != 0) {
            System.out.println("Error: Write Variable by Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Write Variable by Handle!");
        }
   
        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
                
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        return ret;
    }


    /**
     * A method to test the reading and writing of PLC variables.
     * @param args not used.
     */    
     
    public static void main(String[] args) {
    
        PLCServer comm = new PLCServer(801);
        
        try {
            
            String clientSentence;
            String capSentence;
            ServerSocket welcome = new ServerSocket(ethPort);
            String returnVal;
            ReturnValue retn;
            
            System.out.println(prefix+"PLC IP Address: "+Inet4Address.getLocalHost().getHostAddress());
            System.out.println(prefix+"Listening on port "+ethPort);
            System.out.println(prefix+"Waiting for new request...");
            
            Socket conn = welcome.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());      
            
            for(int i=0; i<MAX_REQUESTS; i++) {
                                
                returnVal = "";
                clientSentence = in.readLine();
                String currTime = new java.text.SimpleDateFormat("dd.MM.yyyy @ HH:mm:ss").format(new Date());
                System.out.println(prefix+"("+currTime+") Received the following raw request: "+clientSentence);
                
                Request req = getRequest(clientSentence);
                
                if(req == null) {
                    try {
                       out.writeBytes(REQUEST_ERROR+'\n');
                    }
                    catch(Exception e) {
                        System.out.println(prefix+"Socket was closed by remote client");
                        conn.close();
                        System.out.println(prefix+"Waiting for new request..."); 
                        conn = welcome.accept();
                        in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        out = new DataOutputStream(conn.getOutputStream());  
                    }
                }
                else {
                    System.out.println(prefix+"Request was parsed as: "+req.toString());
                                        
                    if(req.operation == Request.WRITE) {
                        switch(req.type) {
                            case Request.BOOL:  retn = comm.writeBool(req.scope+""+req.variable, Boolean.valueOf(req.value)); 
                                                out.writeBytes(req.value+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            case Request.SHORT: retn = comm.writeInt(req.scope+""+req.variable, Short.valueOf(req.value)); 
                                                out.writeBytes(req.value+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            case Request.FLOAT: retn = comm.writeReal(req.scope+""+req.variable, Float.valueOf(req.value)); 
                                                out.writeBytes(req.value+" "+decodeBeckhoffCode(retn.error())+'\n'); break;     
                            case Request.STRING: retn = comm.writeString(req.scope+""+req.variable, req.value); 
                                                out.writeBytes(req.value+" "+decodeBeckhoffCode(retn.error())+'\n'); break;                                       
                        }
                    } 
                    else if(req.operation == Request.READ) {                   
                        switch(req.type) {
                            case Request.BOOL:  retn = comm.readBool(req.scope+""+req.variable); returnVal = Boolean.toString(retn.boolVal); 
                                                out.writeBytes(Boolean.toString(retn.boolVal)+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            case Request.SHORT: retn = comm.readInt(req.scope+""+req.variable); returnVal = Short.toString(retn.shortVal); 
                                                out.writeBytes(Short.toString(retn.shortVal)+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            case Request.FLOAT: retn = comm.readReal(req.scope+""+req.variable); returnVal = Float.toString(retn.realVal); 
                                                out.writeBytes(Float.toString(retn.realVal)+" "+decodeBeckhoffCode(retn.error())+'\n'); break;       
                            case Request.STRING: retn = comm.readString(req.scope+""+req.variable); returnVal = retn.strVal; 
                                                out.writeBytes(retn.strVal); break;   
                        }                    
                    }

                    System.out.println(prefix+"Waiting for new request..."); 
                }                               
            }
            
        } catch (Exception e) {
            System.out.println("Error: Close program");
            e.printStackTrace();
        }
    }
        

    /**
     * Create an instance of a 'Request' class based on the
     * message that was received from the client/application.
     *
     * @param line The received message as a string.
     * @return The message as an object of the 'Request' class.  
     */  
     
    public static Request getRequest(String line) {
    
        Request req = new Request();
        String clause;
        Integer clauseInt, temp;
        
        try {
            
            StringTokenizer tok = new StringTokenizer(line);

            // first token should be READ/WRITE, e.g. READ = 1
            clause = tok.nextToken();    
            clauseInt = new Integer(clause);
            req.operation = clauseInt.intValue();
            temp = clauseInt;
            
            // second token should be type (e.g. boolean = 2)
            clause = tok.nextToken();
            clauseInt = new Integer(clause);
            req.type = clauseInt.intValue();

            if(temp.intValue() == Request.WRITE) {
               clause = tok.nextToken();
               req.value = new String(clause);
            }
        
            clause = tok.nextToken();
            if(clause.equalsIgnoreCase("GLOBAL")) {
                req.scope = Request.GLOBAL;
            }
            else {
                req.scope = Request.LOCAL;
            }
        
            clause = tok.nextToken();
            req.variable = clause;
        } catch (Exception e) {
            System.out.println(prefix+"Error while parsing request");
            return null;
        }
        return req;
    }
            

    /**
     * Convert Beckhoff error codes into readable text.
     *
     * @param codeStr The error code as a string.
     * @return The textual version of the code. 
     */  
     
    public static String decodeBeckhoffCode(String codeStr) {
        
        int code = new Integer(codeStr);
        
        switch(code) {
            case 6: return "ERROR_TWINCAT_NOT_STARTED"; 
            case 707: return "ERROR_PLC_PROGRAM_NOT_LOADED"; 
            case 710: return "ERROR_VARIABLE_NOT_FOUND_ON_PLC_OR_OUT_OF_SCOPE";
            case 0: return "SUCCESS";
            
            // TODO: Check if the following is correct.
            // case 711: return "ERROR_VARIABLE_SYMBOL_NOT_FOUND";     
        }

        return "UNKNOWN_ERROR";
    }
    
    
    /**
     * An inner class representing the result of 
     * a read/write request executed on the PLC.
     */  
     
    class ReturnValue {
    
        public String strVal;
        public short shortVal;
        public boolean boolVal;
        public long errCode;
        public float realVal;
        
        public String error() {
            return Long.toHexString(errCode);
        }        
    }
}

