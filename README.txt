----------------------------------------------
This repository contains the following folders 
----------------------------------------------

plcServer/
	This folder contains a library for reading/writing PLC 
	variables, and a server based on Java sockets, which 
	listens for command-requests from a client, forwards 
	them down to the PLC, and sends back the data (if any) 
	and result to the client. 
	
	This folder should be placed on a Beckhoff PLC. 

	
plcClient/
	This folder has a library that can be used to establish
	a connection with a 'plcServer' running on a remote PLC, 
	send it requests, get back replies/results, and disconnect
	from the server.

	This folder should be placed on a Raspberry Pi or any other 
	device that is connected to the PLC via an Ethernet cable.

	
examples/
	This folder has a basic Jade agent that establishes a 
	connection with the PLC, reads from and writes to PLC data 
	structures, and receives and interprets replies from the 
	PLC. The agent uses the plcClient-library above.


platforms/
	This folder contains platform-specific code. The 'smc'
	subfolder has scripts to start and run a Jade agent (each 
	of which can be one of 5 types) for each production cell
	in the SMART-SMC platform at the University of Nottingham.


jars/
	This folder has a jar file for running the Jade multi-agent
	system and communications middleware, and the Beckhoff-supplied
	'TcJavaToAds' jar file, which allows access to Beckhoff's DLL.

	
------------------
Setup instructions
------------------

* Move the AdsToJava.dll file (provided by Beckhoff's TwinCAT 
  package) into the Windows\System32 folder on the Beckhoff PLC
  
* Install the Java Runtime Environment (JRE) on the PLC

* Set the PLC's IP address; e.g: 'sudo ifconfig eth0 192.168.1.4'

* Run the server; e.g.: 'java -jar PLCServer.jar'

* Run the Java/Jade client similarly


------
Videos
------

Some videos of how this interface has been used to install and 
run a communications middleware and multi-agent system can be 
viewed at the following URLs:

* https://www.youtube.com/watch?v=9sQRYxLk5CE

* https://www.youtube.com/watch?v=XPgfIwXXlus


-------
Contact
-------

Current developer/maintainer:
Lavindra de Silva [lavindra.desilva@nottingham.ac.uk]

