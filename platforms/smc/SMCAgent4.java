/**
 *
 *
 */

import plcClient.*;
import java.io.*;
import java.net.*;
import java.util.*;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**
 * <code>OneShotBehaviour</code>. When the generic behaviour completes the agent terminates.
 * @author Lavindra de Silva 
 */

public class SMCAgent4 extends Agent {

  private String request, reply;
  private static final String hardCodedIP = "192.168.1.3"; // IP to use is printed by 'JavaSMC' server running on the PLC
  private static final int defaultPort = 850;              // the port is fixed to 850
  private static final long timeStep = 3000;              
  private Message req, msgReply;
  private boolean verbose = true;
  private PLCClient client;
  private String prefix;
  private int station;


  protected void setup() {

    Object[] args = getArguments();
    station = Integer.parseInt((String)args[0]);

    prefix =  "["+getLocalName()+"] "; 
    System.out.println(prefix+"started");
    
    // Add the CyclicBehaviour
    addBehaviour(new CyclicBehaviour(this) {
      public void action() {
        //System.out.println("Cycling...");
      } 
    });

    // Add the generic behaviour
    addBehaviour(new PoolingBehaviour(this,timeStep));

    // this 'client' object is our handle to the PLC
    try {
      client = new PLCClient(hardCodedIP, defaultPort, verbose);

      while(true) {
        // connect via ethernet cable to the 'JavaSMC' server listening on the PLC
        if(!client.connectToPLC()) {
          System.out.println("Could not open a socket connection with the JavaSMC-server on the PLC");
	  Thread.sleep(5000);
        }
        else
          break;
      }
    }
    catch(Exception e) {
      System.out.println("Could not open a socket connection with the JavaSMC-server on the PLC");
      System.exit(0);
    }
  } 



  /**
   * Inner class PoolingBehaviour
   */

  private class PoolingBehaviour extends TickerBehaviour {
    private int step = 0;
    private String lastBarCode = " ";
    private Random randomNum;
    private String labelName;

    public PoolingBehaviour(Agent ag, long time) {
      super(ag,time);
    }

    @Override
    protected void onTick() {

      System.out.println(prefix+"tick");

      try {

        // send a request to read the string from the PLC's local variable named 'LastBarCode'
        req = new Message(Message.READ, Message.STRING, Message.LOCAL, "LastBarcode");
        reply = client.send(req.toString());
	Message replyMsg = new Message(reply);
        interpret(replyMsg);
	String barCode = replyMsg.getValue();
	
 	// if the barcode has changed
	if( !lastBarCode.equalsIgnoreCase(barCode) ) {
          lastBarCode = barCode;

      	  // need to get automatically via RTI
          randomNum = new Random();

	  // for now everything's tested
      	  //int loadMode = randomNum.nextInt(2);
	  int loadMode = 0;

	  // put on pallet
	  if(loadMode == 0) {
            req = new Message(Message.WRITE, Message.BOOL, String.valueOf(true), Message.LOCAL, "Go_Lid");
            reply = client.send(req.toString());
	    replyMsg = new Message(reply);
            interpret(replyMsg);

	    //labelName = "HIWORLD" + step;
	    FindAction find = new FindAction();
	    labelName = find.getValue(barCode,station);

            req = new Message(Message.WRITE, Message.STRING, labelName, Message.LOCAL, "LabelName");
            reply = client.send(req.toString());
	    replyMsg = new Message(reply);
            interpret(replyMsg);

            req = new Message(Message.WRITE, Message.BOOL, String.valueOf(false), Message.LOCAL, "Go_Passthrough");
            reply = client.send(req.toString());
	    replyMsg = new Message(reply);
            interpret(replyMsg);
          }

	  // allow to pass through
          else {
            req = new Message(Message.WRITE, Message.BOOL, String.valueOf(false), Message.LOCAL, "Go_Lid");
            reply = client.send(req.toString());
	    replyMsg = new Message(reply);
            interpret(replyMsg);

            req = new Message(Message.WRITE, Message.BOOL, String.valueOf(true), Message.LOCAL, "Go_Passthrough");
            reply = client.send(req.toString());
	    replyMsg = new Message(reply);
            interpret(replyMsg);
          }

	  // pulse the boolean variable `Go' 
          req = new Message(Message.WRITE, Message.BOOL, String.valueOf(true), Message.LOCAL, "Go");
          reply = client.send(req.toString());
	  replyMsg = new Message(reply);
          interpret(replyMsg);

	  Thread.sleep(2000);

          req = new Message(Message.WRITE, Message.BOOL, String.valueOf(false), Message.LOCAL, "Go");
          reply = client.send(req.toString());
	  replyMsg = new Message(reply);
          interpret(replyMsg);
        }
      }
      catch(Exception e) {
        System.out.println("Could not communicate with PLC");
      }

      step++;
    } 



    //public boolean done() {
    //  return step == 5;
    //} 



    public int onEnd() {
      client.disconnectFromPLC();
      myAgent.doDelete();
      return super.onEnd();
    } 
  }



  private void interpret(Message msgReply) {

    if(msgReply.getResult() == Message.ERROR) {
      System.out.println("Operation could not be successfully performed on the PLC");
    }
    else if(msgReply.getResult() == Message.SUCCESS) {
      System.out.println("Operation was successful on the PLC, with value: " + msgReply.getValue());
    }
  }  

}
