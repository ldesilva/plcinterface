import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.*;
import java.util.*;
 
public class FindAction {

  String excelFile = "actions.txt";
  int testValue = 1;
 
  public String getValue(String scannedBarcode, int station) {
 
    try {
      BufferedReader br = new BufferedReader(new FileReader(excelFile));
 
      String line;
 
      while ((line = br.readLine()) != null) {

	//System.out.println("here's the line "+line);
        StringTokenizer tokens = new StringTokenizer(line, "\0\n\t ");
        String token = null;
        String barCode;

	// first token should be the barcode
        barCode = tokens.nextToken();
	//System.out.println("here's the passed"+scannedBarCode);
	//System.out.println("here's the barcode"+barCode);

	if(barCode.equals(scannedBarcode)) {

	  int column = -1;
	  testValue = 1;

	  switch(station) {
	
 	    case 4: column = 2; break;
 	    case 2: column = 3; break;
 	    case 3: column = 4; break;
 	    case 6: column = 5; break;
 	    case 5: column = 6; break;
 	    case 9: column = 7; break;
	  }
	
	  int total = 0;

	  for(int i=1; i < column; i++) {
            token = tokens.nextToken();

	    // add up 2nd, 3rd and 4th columns if Station6 
	    if((station == 6 ||
                station == 5) && i <= 3) {
  	      total += Integer.parseInt(token);
	    }   
	  }

	  if(total != 0) testValue = total;

	  // need to pad upto 11 chars
	  if(station == 9) {
	    //return String.format("%1$-" + 11 + "s", token); 
	    return String.format("%-11s", token).replace(' ', '*');
	  }

	  return token;
	}
      }
    } 
    catch (IOException e) {
      e.printStackTrace();
    } 

    return null;
  }



  public int getTestVal() {
    return testValue;
  }



  // test method

  public static void main (String [] args) {

    FindAction test = new FindAction();
    System.out.println(test.getValue("33340302", 5));
    System.out.println("Test value:"+test.getTestVal());
    System.out.println(test.getValue("33340005", 3));
    System.out.println("HERE IT IS:"+test.getValue("33340098", 9)+":END");
    System.out.println(test.getValue("33340333", 2));
    System.out.println("Test value:"+test.getTestVal());
    System.out.println(test.getValue("33340227", 4));
    System.out.println(test.getValue("33340081", 5));
    System.out.println(test.getValue("33340142", 5));
    System.out.println("Test value:"+test.getTestVal());
    System.out.println("HERE IT IS:"+test.getValue("33340173", 9)+":END");

    System.out.println(test.getValue("33340234", 5));
    System.out.println("Test value:"+test.getTestVal());
  }
}
