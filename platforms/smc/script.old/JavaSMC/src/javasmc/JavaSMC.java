/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javasmc;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import de.beckhoff.jni.Convert;
import de.beckhoff.jni.JNIByteBuffer;
import de.beckhoff.jni.tcads.AmsAddr;
import de.beckhoff.jni.tcads.AdsCallDllFunction;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *
 * @author 
 */
public class JavaSMC {
    private static int port = 0;
    private static final int ethPort = 850;
    private static final String prefix = "[Java PLC Server] ";
    private static final String REQUEST_ERROR = "ERROR_INVALID_REQUEST";
    private static final int MAX_REQUESTS = 1000;
    
    //AmsAddr addr= new AmsAddr();
    
    public JavaSMC(int port) {
        this.port = port;
    }
    
    public ReturnValue readBool(String plcVar) {
        boolean ret = false;
        long err;
        ReturnValue retn = new ReturnValue();

        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        //JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar, true));
        JNIByteBuffer dataBuff = new JNIByteBuffer(1);        

        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        //addr.setPort(AdsCallDllFunction.AMSPORT_R0_PLC_RTS1);
        addr.setPort(port);
        
        // Get handle by symbol name
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_HNDBYNAME,
                0x0,
                handleBuff.getUsedBytesCount(),
                handleBuff,
                symbolBuff.getUsedBytesCount(),
                symbolBuff);
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }

        retn.errCode = err;

        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());
        // Read value by handle
        err = AdsCallDllFunction.adsSyncReadReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_VALBYHND,
                hdlBuffToInt,
                0x4,
                dataBuff);
        if (err != 0) {
            System.out.println("Error: Read by handle: 0x" + Long.toHexString(err));
        } else {
            ret = Convert.ByteArrToBool(dataBuff.getByteArray());    
        }

        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        //retn.errCode = err;
        retn.boolVal = ret;
        return retn;
    }

    public ReturnValue readInt(String plcVar) {
        short ret = 0;
        long err;
        ReturnValue retn = new ReturnValue();
        
        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        JNIByteBuffer dataBuff = new JNIByteBuffer(2);        

        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        //addr.setPort(AdsCallDllFunction.AMSPORT_R0_PLC_RTS1);
        addr.setPort(port);
        
        // Get handle by symbol name
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_HNDBYNAME,
                0x0,
                handleBuff.getUsedBytesCount(),
                handleBuff,
                symbolBuff.getUsedBytesCount(),
                symbolBuff);
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        retn.errCode = err;

        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());
        // Read value by handle
        err = AdsCallDllFunction.adsSyncReadReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_VALBYHND,
                hdlBuffToInt,
                0x4,
                dataBuff);
        if (err != 0) {
            System.out.println("Error: Read by handle: 0x" + Long.toHexString(err));
        } else {      
            ret = Convert.ByteArrToShort(dataBuff.getByteArray());             
        }

        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        //retn.errCode = err;
        retn.shortVal = ret;
        return retn;

    }
    
    public ReturnValue writeBool(String plcVar, boolean value) {
        long err;
        ReturnValue ret = new ReturnValue();
        
        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        //JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        //JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar));
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar, true));
        JNIByteBuffer dataBuff = new JNIByteBuffer(Convert.BoolToByteArr(value));     
        
        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        //addr.setPort(AdsCallDllFunction.AMSPORT_R0_PLC_RTS1);
        addr.setPort(port);
        
        //get handle by symbol name 
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr, 0xF003, 0x0,
                handleBuff.getUsedBytesCount(), handleBuff, //buffer for getting handle
                symbolBuff.getUsedBytesCount(), symbolBuff); //buffer containg symbolpath
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        ret.errCode = err;
   
        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());

        //write variable by handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr, 0xF005, hdlBuffToInt,
                dataBuff.getUsedBytesCount(), dataBuff);
   
        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        return ret;
    }
    
    public ReturnValue writeInt(String plcVar, short value) {
        long err;
        ReturnValue ret = new ReturnValue();

        AmsAddr addr = new AmsAddr();
        JNIByteBuffer handleBuff = new JNIByteBuffer(Integer.SIZE / Byte.SIZE);
        JNIByteBuffer symbolBuff = new JNIByteBuffer(Convert.StringToByteArr(plcVar, true));
        JNIByteBuffer dataBuff = new JNIByteBuffer(Convert.ShortToByteArr(value));     
        
        // Open communication
        AdsCallDllFunction.adsPortOpen();
        AdsCallDllFunction.getLocalAddress(addr);
        //addr.setPort(AdsCallDllFunction.AMSPORT_R0_PLC_RTS1);
        addr.setPort(port);
        
        //get handle by symbol name 
        err = AdsCallDllFunction.adsSyncReadWriteReq(addr, 0xF003, 0x0,
                handleBuff.getUsedBytesCount(), handleBuff, //buffer for getting handle
                symbolBuff.getUsedBytesCount(), symbolBuff); //buffer containg symbolpath
        if (err != 0) {
            System.out.println("Error: Get handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Get handle!");
        }
        
        ret.errCode = err;
   
        // Handle: byte[] to int
        int hdlBuffToInt = Convert.ByteArrToInt(handleBuff.getByteArray());

        //write variable by handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr, 0xF005, hdlBuffToInt,
                dataBuff.getUsedBytesCount(), dataBuff);
   
        // Release handle
        err = AdsCallDllFunction.adsSyncWriteReq(addr,
                AdsCallDllFunction.ADSIGRP_SYM_RELEASEHND,
                0,
                handleBuff.getUsedBytesCount(),
                handleBuff);
        if (err != 0) {
            System.out.println("Error: Release Handle: 0x" + Long.toHexString(err));
        } else {
            System.out.println("Success: Release Handle!");
        }

        // Close communication
        err = AdsCallDllFunction.adsPortClose();
        if (err != 0) {
            System.out.println("Error: Close Communication: 0x" + Long.toHexString(err));
        }
        
        return ret;
    }

    public static void main(String[] args) {
        JavaSMC comm = new JavaSMC(801);
        
        try {

            //System.out.println("Reading new values...");
            //boolean b = comm.readBool("MAIN.bool_var");
            //System.out.println("Output: GVL.bool_var: " + b);
            //short s = comm.readInt("MAIN.int_var");
            //System.out.println("Success: GVL value: " + s);                                                        
            //System.out.println("Writing new values...");
            //comm.writeBool("Global_Variables.bool_var", true);
            //comm.writeInt("GVL.TestVar_I3", (short)20);            
            //b = comm.readBool("GVL.TestVar_B1");
            //System.out.println("Success: GVL.TestVar_B1 value: " + b);
            //s = comm.readInt("GVL.TestVar_I3");
            //System.out.println("Success: GVL.TestVar_I3 value: " + s
            
            
            String clientSentence;
            String capSentence;
            ServerSocket welcome = new ServerSocket(ethPort);
            String returnVal;
            ReturnValue retn;
            
            System.out.println(prefix+"PLC IP Address: "+Inet4Address.getLocalHost().getHostAddress());
            System.out.println(prefix+"Listening on port "+ethPort);
            System.out.println(prefix+"Waiting for new request...");
            
            Socket conn = welcome.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());      
            
            for(int i=0; i<MAX_REQUESTS; i++) {
                
                //System.out.println(prefix+"Waiting for new request...");
                
                returnVal = "";
                //conn = welcome.accept();
                //in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                //out = new DataOutputStream(conn.getOutputStream());
                clientSentence = in.readLine();
                String currTime = new java.text.SimpleDateFormat("dd.MM.yyyy @ HH:mm:ss").format(new Date());
                System.out.println(prefix+"("+currTime+") Received the following raw request: "+clientSentence);
                
                Request req = getRequest(clientSentence);
                
                if(req == null) {
                    try {
                       out.writeBytes(REQUEST_ERROR+'\n');
                    }
                    catch(Exception e) {
                        System.out.println(prefix+"Socket was closed by remote client");
                        conn.close();
                        System.out.println(prefix+"Waiting for new request..."); 
                        conn = welcome.accept();
                        in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        out = new DataOutputStream(conn.getOutputStream());  
                        //System.exit(0);
                    }
                }
                else {
                    System.out.println(prefix+"Request was parsed as: "+req.toString());
                    
                    //executeRequest(req);
                    
                    if(req.operation == Request.WRITE) {
                        switch(req.type) {
                            case Request.BOOL:  retn = comm.writeBool(req.scope+"."+req.variable, Boolean.valueOf(req.value)); 
                                                out.writeBytes(req.value+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            case Request.SHORT: retn = comm.writeInt(req.scope+"."+req.variable, Short.valueOf(req.value)); 
                                                out.writeBytes(req.value+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            //case Request.STRING: comm.writeString(req.scope+"."+req.variable, req.value); break;
                        }
                    } 
                    else if(req.operation == Request.READ) {                   
                        switch(req.type) {
                            case Request.BOOL:  retn = comm.readBool(req.scope+"."+req.variable); returnVal = Boolean.toString(retn.boolVal); 
                                                out.writeBytes(Boolean.toString(retn.boolVal)+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            case Request.SHORT: retn = comm.readInt(req.scope+"."+req.variable); returnVal = Short.toString(retn.shortVal); 
                                                out.writeBytes(Short.toString(retn.shortVal)+" "+decodeBeckhoffCode(retn.error())+'\n'); break;
                            //case Request.STRING: comm.writeString(req.scope+"."+req.variable, req.value); break;
                        }                    
                    }
                    //out.writeBytes(returnVal);
                    System.out.println(prefix+"Waiting for new request..."); 
                }                               
            }
            
            //System.in.read();
        } catch (Exception e) {
            System.out.println("Error: Close program");
            e.printStackTrace();
        }
        //}
    }
    
    public static Request getRequest(String line) {
        Request req = new Request();
        String clause;
        Integer clauseInt, temp;
        
        try {
            
            StringTokenizer tok = new StringTokenizer(line);

            // first token should be READ/WRITE, e.g. READ = 1
            clause = tok.nextToken();    
            clauseInt = new Integer(clause);
            req.operation = clauseInt.intValue();
            temp = clauseInt;
            
            // second token should be type (e.g. boolean = 2)
            clause = tok.nextToken();
            clauseInt = new Integer(clause);
            req.type = clauseInt.intValue();

            if(temp.intValue() == Request.WRITE) {
               clause = tok.nextToken();
               req.value = new String(clause);
            }
        
            clause = tok.nextToken();
            if(clause.equalsIgnoreCase("GLOBAL")) {
                req.scope = Request.GLOBAL;
            }
            else {
                req.scope = Request.LOCAL;
            }
        
            clause = tok.nextToken();
            req.variable = clause;
        } catch (Exception e) {
            System.out.println(prefix+"Error while parsing request");
            //e.printStackTrace();
            return null;
        }
        return req;
    }
    
    public static String decodeBeckhoffCode(String codeStr) {
        
        int code = new Integer(codeStr);
        
        switch(code) {
            case 6: return "ERROR_TWINCAT_NOT_STARTED"; 
            case 707: return "ERROR_PLC_PROGRAM_NOT_LOADED"; 
            case 710: return "ERROR_VARIABLE_NOT_FOUND_ON_PLC_OR_OUT_OF_SCOPE";
            case 0: return "SUCCESS";
            //case 711: return "ERROR_VARIABLE_SYMBOL_NOT_FOUND???";     
        }

        return "UNKNOWN_ERROR";
    }
    class ReturnValue {
        public String strVal;
        public short shortVal;
        public boolean boolVal;
        public long errCode;
        
        public String error() {
            //return "test";
            return Long.toHexString(errCode);
        }
        
    }
}

