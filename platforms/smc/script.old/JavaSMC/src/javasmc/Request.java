package javasmc;

public class Request {
    public static final int WRITE = 0;
    public static final int READ = 1;
    
    public static final int BOOL = 2;
    public static final int SHORT = 3;
    public static final int STRING = 4;
    public static final int INT = 5;
    
    public static final String GLOBAL = "Global_Variables";
    public static final String LOCAL = "MAIN";
    
    public int operation;
    public int type;
    public String scope;
    public String variable;
    public String value;
    
    private String toStr = "";
    
    public String toString() {
        switch(operation) {
            case WRITE: toStr += "write"; break;
            case READ: toStr += "read"; break;
        }
        
        toStr += " the ";
                
        switch(type) {
            case BOOL: toStr += "boolean"; break;
            case SHORT: toStr += "short"; break;
            case STRING: toStr += "string"; break;
            case INT: toStr += "integer"; break;                
        }

        if(value != null)
            toStr += " value '" + value + "' to the ";
        else
            toStr += " value from the ";
        
        switch(scope) {
            case GLOBAL: toStr += "global"; break;
            case LOCAL: toStr += "local"; break;
        }
        
        toStr += " variable named '" + variable +"'";

        return toStr;
    }
}