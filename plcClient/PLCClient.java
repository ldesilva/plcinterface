/**
 * PLCClient.java
 *
 * This class is the interface between a Java application
 * and a Java server that runs on a Beckhoff PLC. 
 *
 * @author Lavindra de Silva
 * @version 1.0 
 */
 
package plcClient;

import java.io.*;
import java.net.*;

public class PLCClient {

    private boolean verbose;
    private String IP;
    private int port;
    
    private Socket PLCSocket;
    private DataOutputStream outToServer;
    private BufferedReader inFromServer;
  
    private static final String prefix = "[PLC Client Library] ";

 
     /**
     * Default constructor.
     * @param IP The IP address of the PLC.
     * @param port The port which listens for connections from applications.
     */
     
    public PLCClient(String IP, int port) throws Exception {
    
       this.IP = IP;
       this.port = port;
       
       outToServer = null;
       inFromServer = null;
       verbose = false;
       
       if(verbose) System.out.println(prefix+"Server IP Address: "+IP+". Server port:"+port);
       printIPAddress();
    }


    /**
     * Constructor for debugging purposes.
     * @param IP The IP address of the PLC.
     * @param port The port which listens for connections from applications.
     * @param verbose Whether to print out debug information.
     */
     
    public PLCClient(String IP, int port, boolean verbose) throws Exception {
    
       this.IP = IP;
       this.port = port;
       
       outToServer = null;
       inFromServer = null;
       this.verbose = verbose;
       
       if(verbose) System.out.println(prefix+"Server IP Address: "+IP+". Server port:"+port);
       printIPAddress();
    }


    /**
     * Open a socket connection with the PLC and initialise input/output streams.
     * @return True if connection was established and false otherwise.
     */
     
    public boolean connectToPLC() {

       if(verbose) System.out.println(prefix+"Connecting to PLC...");
       if(verbose) System.out.println(prefix+"(If stuck here, check ethernet cable and whether JavaSMC server is running on PLC)");

       try {
          PLCSocket = new Socket(IP, port);
          outToServer = new DataOutputStream(PLCSocket.getOutputStream());
          inFromServer = new BufferedReader(new InputStreamReader(PLCSocket.getInputStream()));
       }
       catch(Exception e) {
	      if(verbose) System.out.println(prefix+"Could not open a socket connection with JavaSMC server on PLC");
          if(verbose) System.out.println(prefix+"(Have you done 'sudo ifconfig eth0 192.168.1.4'?)");
          if(verbose) System.out.println(prefix+"(If so, try pinging the PLC ethernet port (192.168.1.2?)");

          return false;	
       }
       return true;
    }


    /**
     * Safely close the previously established socket connection with the PLC.
     * @return True if connection was established and false otherwise.
     */
     
    public boolean disconnectFromPLC() {

       try {
          PLCSocket.shutdownInput();
          PLCSocket.close();
       }
       catch(Exception e) {
          return false;	
       }
       return true;	
    }


    /**
     * Send a message to the PLC's Java server requesting a
     * data structure to be read or written to (or indirectly
     * requesting an action to be performed).
     *
     * @param request The message to be sent.
     * @return The result of the read/write/action.
     */
     
    public String send(String request) throws Exception {

	   boolean wasConnected = true;

	   if(PLCSocket == null) {
	      wasConnected = false;
	      connectToPLC();
	   }

	   if(verbose) System.out.println(prefix+"Sending the following request: "+request);
       outToServer.writeBytes(request + '\n');
       String reply = inFromServer.readLine();
        
	   if(verbose) System.out.println(prefix+"Received the following reply: "+reply);

	   if(!wasConnected) 
	      disconnectFromPLC();

       return reply;
    }


    /**
     * Print out the client (application) IP address.
     */
     
    private void printIPAddress() {
    
	   try {
	      if(verbose) System.out.println(prefix+"Client IP Address: "+Inet4Address.getLocalHost().getHostAddress());
	   }
	   catch(Exception e) {
	      if(verbose) System.out.println(prefix+"Could not obtain client IP Address");
	   }
    }
}
