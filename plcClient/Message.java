/**
 * Message.java
 *
 * This class represents a message that is passed
 * between Java applications and Beckhoff PLCs.
 *
 * @author Lavindra de Silva
 * @version 1.0 
 */
 
package plcClient;

import java.util.*;

public class Message {

   /**
    * The types of messages, the kinds of data
    * structures that can be manipulated, etc.
    */
    
   public static final int WRITE = 0;
   public static final int READ = 1; 
   
   public static final int BOOL = 2; 
   public static final int SHORT = 3; 
   public static final int STRING = 4; 
   public static final int INT = 5; 
   public static final int FLOAT = 6; 
   
   public static final String GLOBAL = "global"; 
   public static final String LOCAL = "local"; 
   public static final String SPACE = " "; 
   private static final String ERROR_STR = "ERROR"; 

   public static final int SUCCESS = 10; 
   public static final int ERROR = 11; 

   private int type, replyResult, operation;
   private String scope, var, value, replyValue;


   /**
    * The constructor for a message of type 'reply'.
    * @param result The result of sending a read/write/actuate request to the PLC.
    */
     
   public Message(String result) {

      StringTokenizer tokens = new StringTokenizer(result, "\0\n ");
      String token, replyRes; 

      try {
         replyValue = tokens.nextToken();
         replyRes = tokens.nextToken();

         if(replyRes.toUpperCase().contains(ERROR_STR)) {
            replyResult = ERROR;   
	     }
	     else {
            replyResult = SUCCESS;   
	     }
      }
      catch(Exception e) {
         System.out.println("This message does not have the right syntax.");
      }
   }


   /**
    * The constructor for a message of type 'request'.
    * @param operation Whether this is a read or write operation.
    * @param type The variable's type (e.g. int).
    * @param value The value to be written to the variable.
    * @param scope Whether the variable is global or local.
    * @param var The name of the variable.
    */
     
   public Message(int operation, int type, String value, String scope, String var) {

      this.operation = operation;
      this.type = type;
      this.value = value;
      this.scope = scope;
      this.var = var;
   } 


   /**
    * The constructor for a message of type 'request'.
    * @param operation Whether this is a read or write operation.
    * @param type The variable's type (e.g. int).
    * @param scope Whether the variable is global or local.
    * @param var The name of the variable.
    */
    
   public Message(int operation, int type, String scope, String var) {
   
      this.operation = operation;
      this.type = type;
      this.value = null;
      this.scope = scope;
      this.var = var;
   }


   /**
    * Convert this message object into a string.
    */
    
   public String toString() {

      if(value == null) 
         return operation + SPACE + type + SPACE + scope + SPACE + var;
      else
         return operation + SPACE + type + SPACE + value + SPACE + scope + SPACE + var;
   }


   /**
    * Get the result (success/failure) of this request.
    */
    
   public int getResult() {
   
      return replyResult;
   }


   /**
    * Get the contents of the data structure that was read.
    */
    
   public String getValue() {
   
      return replyValue;
   }
}
