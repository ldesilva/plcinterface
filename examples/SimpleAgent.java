
import plcClient.*;
import java.io.*;
import java.net.*;
import java.util.*;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**
 * <code>OneShotBehaviour</code>. When the generic behaviour completes the agent terminates.
 * @author Lavindra de Silva 
 */

public class SimpleAgent extends Agent {

  private String request, reply;
  private static final String hardCodedIP = "192.168.1.2"; // IP to use is printed by 'JavaSMC' server running on the PLC
  private static final int defaultPort = 850;              // the port is fixed to 850
  private static final long timeStep = 15000;              
  private Message req, msgReply;
  private boolean verbose = true;
  private PLCClient client;
  private String prefix;



  protected void setup() {
    prefix =  "["+getLocalName()+"] "; 
    System.out.println(prefix+"started");
    
    // Add the CyclicBehaviour
    addBehaviour(new CyclicBehaviour(this) {
      public void action() {
        //System.out.println("Cycling...");
      } 
    });

    // Add the generic behaviour
    addBehaviour(new FourStepBehaviour(this,timeStep));

    // this 'client' object is our handle to the PLC
    try {
      client = new PLCClient(hardCodedIP, defaultPort, verbose);

      // connect via ethernet cable to the 'JavaSMC' server listening on the PLC
      if(!client.connectToPLC()) {
        System.out.println("Could not open a socket connection with the JavaSMC-server on the PLC");
        System.exit(0);
      }
    }
    catch(Exception e) {
      System.out.println("Could not open a socket connection with the JavaSMC-server on the PLC");
      System.exit(0);
    }
  } 



  /**
   * Inner class FourStepBehaviour
   */

  private class FourStepBehaviour extends TickerBehaviour {
    private int step = 0;
    private boolean testBool;

    public FourStepBehaviour(Agent ag, long time) {
      super(ag,time);
    }

    @Override
    protected void onTick() {

      System.out.println(prefix+"tick");

      try {
        switch (step % 5) {
        case 0:

	  if(testBool) testBool = false;
          else testBool = true;

          // create a request to write the boolean value 'true' to the PLC's local variable named 'dummy_bool'
          req = new Message(Message.WRITE, Message.BOOL, String.valueOf(testBool), Message.GLOBAL, "dummy_Gbool");

          // send the request to the JavaSMC server and get its reply
          reply = client.send(req.toString());

          msgReply = new Message(reply);
          interpret(msgReply);
          break;

        case 1:
          req = new Message(Message.WRITE, Message.STRING, "testing123", Message.LOCAL, "dummy_str");
          // send the request to the JavaSMC server and get its reply
          reply = client.send(req.toString());

          msgReply = new Message(reply);
          interpret(msgReply);
          break;

        case 2:
          req = new Message(Message.WRITE, Message.FLOAT, String.valueOf(step+12345.6789), Message.LOCAL, "dummy_real");
          reply = client.send(req.toString());
          interpret(new Message(reply));
          break;

        case 3:
          // send a request to read the short (int) value from the PLC's local variable named 'int_counter'
          req = new Message(Message.READ, Message.FLOAT, Message.LOCAL, "real_counter");
          reply = client.send(req.toString());
          interpret(new Message(reply));
          break;

        case 4:
          // send a request to read the short (int) value from the PLC's local variable named 'int_counter'
          req = new Message(Message.READ, Message.STRING, Message.LOCAL, "dummy_str");
          reply = client.send(req.toString());
          interpret(new Message(reply));
          break;

        }
      }
      catch(Exception e) {
        System.out.println("Could not communicate with PLC");
      }

      step++;
    } 



    //public boolean done() {
    //  return step == 5;
    //} 



    public int onEnd() {
      client.disconnectFromPLC();
      myAgent.doDelete();
      return super.onEnd();
    } 
  }


  private void interpret(Message msgReply) {

    if(msgReply.getResult() == Message.ERROR) {
      System.out.println("Operation could not be successfully performed on the PLC");
    }
    else if(msgReply.getResult() == Message.SUCCESS) {
      System.out.println("Operation was successful on the PLC, with value: " + msgReply.getValue());
    }
  }  
}
